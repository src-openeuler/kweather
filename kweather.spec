Name:           kweather 
Version:        23.08.5
Release:        2
License:        GPLv2+
Summary:        Convergent KDE weather application
Url:            https://invent.kde.org/plasma-mobile/kweather
Source0:        https://download.kde.org/stable/release-service/%{version}/src/%{name}-%{version}.tar.xz

BuildRequires:  appstream
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  extra-cmake-modules
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  kf5-rpm-macros
BuildRequires:  libappstream-glib

BuildRequires:  qt5-qtcharts qt5-qtcharts-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtdeclarative-devel
BuildRequires:  qt5-qtquickcontrols2-devel
BuildRequires:  qt5-qtsvg-devel

BuildRequires:  cmake(KF5Config)
BuildRequires:  cmake(KF5CoreAddons)
BuildRequires:  cmake(KF5I18n)
BuildRequires:  cmake(KF5KWeatherCore)
BuildRequires:  cmake(KF5Kirigami2)
BuildRequires:  cmake(KF5KirigamiAddons)
BuildRequires:  cmake(KF5Notifications)
BuildRequires:  cmake(KF5Plasma)
BuildRequires:  cmake(KF5QuickCharts)

Requires:       kf5-kirigami2
Requires:       kf5-kirigami2-addons
Requires:       hicolor-icon-theme

%description
Weather application for Plasma Mobile

%prep
%autosetup -n %{name}-%{version}

%build
%{cmake_kf5}
%cmake_build

%install
%cmake_install

chmod -x %{buildroot}%{_kf5_datadir}/applications/org.kde.%{name}.desktop
%find_lang %{name}

%check
appstreamcli validate --no-net %{buildroot}%{_kf5_datadir}/metainfo/org.kde.%{name}.appdata.xml
desktop-file-validate %{buildroot}%{_kf5_datadir}/applications/org.kde.%{name}.desktop

%files -f %{name}.lang
%license LICENSES/*.txt
%{_kf5_bindir}/%{name}
%{_kf5_datadir}/applications/org.kde.%{name}.desktop
%{_kf5_metainfodir}/org.kde.%{name}.appdata.xml
%{_kf5_metainfodir}/org.kde.plasma.%{name}_1x4.appdata.xml
%{_kf5_datadir}/icons/hicolor/scalable/apps/org.kde.%{name}.svg
%{_kf5_datadir}/dbus-1/services/org.kde.%{name}.service
%{_kf5_datadir}/plasma/plasmoids/org.kde.plasma.%{name}_1x4/contents/ui/LocationSelector.qml
%{_kf5_datadir}/plasma/plasmoids/org.kde.plasma.%{name}_1x4/contents/ui/WeatherContainer.qml
%{_kf5_datadir}/plasma/plasmoids/org.kde.plasma.%{name}_1x4/contents/ui/main.qml
%{_kf5_datadir}/plasma/plasmoids/org.kde.plasma.%{name}_1x4/metadata.json
%{_kf5_datadir}/plasma/plasmoids/org.kde.plasma.%{name}_1x4/metadata.json.license
%{_kf5_qtplugindir}/plasma/applets/plasma_applet_%{name}_1x4.so

%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Tue Jan 09 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.4-1
- update verison to 23.08.4

* Mon Dec 04 2023 misaka00251 <liuxin@iscas.ac.cn> - 23.04.3-1
- Init package
